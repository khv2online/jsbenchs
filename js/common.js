var outlet = document.getElementById('outlet');
const TIMES = 10000;

var words = ["Главная", "Расположение", "Контакты"];
//- START: mithrill
console.time("mithrill");

for (var i = 0; i < TIMES; i++) {
    outlet.innerHTML = '';
    m.render(outlet, m(".modal[data-backdrop='false'][id='mainMenuModal']",
        m(".b-menu", [
            m(".b-menu__bg[data-bg-img-num='1']",
                m(".b-menu__bg-underlay")
            ),
            m(".b-menu__wrap",
                m("ul.b-menu__inner", [
                    m("li.b-menu__leaf.current",
                        m("a[href='/']",
                            words[0]
                        )
                    ),
                    m("li.b-menu__leaf.", [
                        m("a[href='/about/project/']",
                            "О проекте"
                        ),
                        m("ul.b-menu__submenu", [
                            m("li.b-menu__subleaf.",
                                m("a[href='/about/project/']",
                                    "О проекте"
                                )
                            ),
                            m("li.b-menu__subleaf.",
                                m("a[href='/about/location/']",
                                    words[1]
                                )
                            ),
                            m("li.b-menu__subleaf.",
                                m("a[href='/about/diary/']",
                                    "Динамика строительства"
                                )
                            ),
                            m("li.b-menu__subleaf.",
                                m("a[href='/about/gallery/']",
                                    "Галерея"
                                )
                            ),
                            m("li.b-menu__subleaf.",
                                m("a[href='/about/developer/']",
                                    "О застройщике"
                                )
                            ),
                            m("li.b-menu__subleaf.",
                                m("a[href='/about/news/']",
                                    "Новости"
                                )
                            )
                        ])
                    ]),
                    m("li.b-menu__leaf.",
                        m("a[href='/mortgage/']",
                            "Ипотека — 7,4%"
                        )
                    ),
                    m("li.b-menu__leaf.",
                        m("a[href='/flats/']",
                            "Выбрать квартиру"
                        )
                    ),
                    m("li.b-menu__leaf.",
                        m("a[href='/contacts/']",
                            words[2]
                        )
                    )
                ])
            )
        ])
    ));
}

console.timeEnd("mithrill");
//- END: mithrill



//- START: innerHTML
console.time("innerHTML");
 
for (var i = 0; i < TIMES; i++) {
    outlet.innerHTML = '';
    outlet.innerHTML = `<div class="modal" id="mainMenuModal" data-backdrop="false"><div class="b-menu"><div class="b-menu__bg" data-bg-img-num="1"><div class="b-menu__bg-underlay"></div></div><div class="b-menu__wrap"><ul class="b-menu__inner"><li class="b-menu__leaf current"><a href="/">${ words[0]}</a></li><li class="b-menu__leaf "><a href="/about/project/">О проекте</a><ul class="b-menu__submenu"><li class="b-menu__subleaf "><a href="/about/project/">О проекте</a></li><li class="b-menu__subleaf "><a href="/about/location/">${ words[1]}</a></li><li class="b-menu__subleaf "><a href="/about/diary/">Динамика строительства</a></li><li class="b-menu__subleaf "><a href="/about/gallery/">Галерея</a></li><li class="b-menu__subleaf "><a href="/about/developer/">О застройщике</a></li><li class="b-menu__subleaf "><a href="/about/news/">Новости</a></li></ul></li><li class="b-menu__leaf "><a href="/mortgage/">Ипотека — 7,4%</a></li><li class="b-menu__leaf "><a href="/flats/">Выбрать квартиру</a></li><li class="b-menu__leaf "><a href="/contacts/">${ words[2]}</a></li></ul></div></div></div>`;
}

console.timeEnd("innerHTML");
//- END: innerHTML



//- START: hogan

console.time("hogan");
var context = { words: words };
var hoganTemplate = Hogan.compile(`<div class="modal" id="mainMenuModal" data-backdrop="false"><div class="b-menu"><div class="b-menu__bg" data-bg-img-num="1"><div class="b-menu__bg-underlay"></div></div><div class="b-menu__wrap"><ul class="b-menu__inner"><li class="b-menu__leaf current"><a href="/">{{ words.0 }}</a></li><li class="b-menu__leaf "><a href="/about/project/">О проекте</a><ul class="b-menu__submenu"><li class="b-menu__subleaf "><a href="/about/project/">О проекте</a></li><li class="b-menu__subleaf "><a href="/about/location/">{{ words.1 }}</a></li><li class="b-menu__subleaf "><a href="/about/diary/">Динамика строительства</a></li><li class="b-menu__subleaf "><a href="/about/gallery/">Галерея</a></li><li class="b-menu__subleaf "><a href="/about/developer/">О застройщике</a></li><li class="b-menu__subleaf "><a href="/about/news/">Новости</a></li></ul></li><li class="b-menu__leaf "><a href="/mortgage/">Ипотека — 7,4%</a></li><li class="b-menu__leaf "><a href="/flats/">Выбрать квартиру</a></li><li class="b-menu__leaf "><a href="/contacts/">{{ words.2 }}</a></li></ul></div></div></div>`);

for (var i = 0; i < TIMES; i++) {
    outlet.innerHTML = '';
    outlet.innerHTML = hoganTemplate.render(context);
}
console.timeEnd("hogan");
//- END: hogan